import unittest
from ActivitySelector import ActivitySelectorIterative, ActivitySelectorRecursive, SortActivities

class ActivitySelectorTest(unittest.TestCase):
	def testActivitySelectionIterative(self):
		activity = ['A1', 'A2', 'A3', 'A4', 'A5']
		start    = [ 1  ,  6  ,  2  ,  4  ,  8  ]
		finish   = [ 10 ,  8  ,  5  ,  7  ,  11 ]
		activity, start, finish = SortActivities(activity, start, finish)
		self.assertEqual(ActivitySelectorIterative(activity, start, finish), ['A3', 'A2', 'A5'])
	def testActivitySelectionRecursive(self):
		activity = ['A1', 'A2', 'A3', 'A4', 'A5']
		start    = [ 1  ,  6  ,  4  ,  5  ,  9  ]
		finish   = [ 3  ,  8  ,  5  ,  7  ,  11 ]
		activity, start, finish = SortActivities(activity, start, finish)
		self.assertEqual(ActivitySelectorRecursive(activity, start, finish, -1, len(activity)), ['A1', 'A3', 'A4', 'A5'])
	def testAS_IterVsRecur(self):
		activity = ['A1', 'A2', 'A3', 'A4', 'A5']
		start = [2, 3, 1, 5, 10]
		finish = [7, 5, 2, 8, 12]
		A, s, f = SortActivities(activity, start, finish)
		#Assertion test to show both iterative and recursive algorithms select the same sactivities
		self.assertEqual(ActivitySelectorIterative(A, s, f), ActivitySelectorRecursive(A, s, f, -1, len(activity))) 
		#Additional assertion tests to check function SortActivities() correctly sorts Activities, Start and Finish array in ascending order of Finsih times
		self.assertEqual(f, [2, 5, 7, 8, 12])
		self.assertEqual(s, [1, 3, 2, 5, 10])
		self.assertEqual(A, ['A3', 'A2', 'A1', 'A4', 'A5'])
if __name__  == '__main__':
	unittest.main()
