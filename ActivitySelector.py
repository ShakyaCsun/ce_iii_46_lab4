activities = ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10', 'a11']
start = [1, 1, 5, 7, 3, 4, 6, 8, 9, 10, 2]
finish = [4, 5, 6, 9, 8, 5, 11, 12, 15, 14, 9]

def SortActivities(A, s, f):
	for i in range(len(f)):
		key = f[i]
		temp_A = A[i]
		temp_s = s[i]
		j = i

		while j > 0 and f[j - 1] > key:
			f[j] = f[j - 1]
			s[j] = s[j - 1]
			A[j] = A[j - 1]
			j = j - 1
		f[j] = key
		A[j] = temp_A
		s[j] = temp_s
	return A, s, f

def ActivitySelectorIterative(A, s, f):
	S = []
	

	S.append(A[0])
	k = 0
	n = len(A)
	for i in range(n):
		if (s[i] >= f[k]):
			S.append(A[i])
			k = i
	return S

def ActivitySelectorRecursive(A, s, f, k, n):
	S = []
	m = k + 1
	while m < n and s[m] < f[k] and k >= 0:
		m = m + 1
	if m < n:
		return [A[m]] + ActivitySelectorRecursive(A, s, f, m, n)
	else:
		return S

def printStartAndEndTimes (A, s, f, sA): #sA is the array of selected Activities
	i = 0
	while(i < len(sA)):
		for j in range(len(A)):
			if(A[j]==sA[i]):
				print("Activity ", A[j], ": (", s[j], ", ", f[j], ")")
		i = i + 1

if __name__ == '__main__':
	activities, start, finish = SortActivities(activities, start, finish)
	listIter = ActivitySelectorIterative(activities, start, finish)
	listRecur = ActivitySelectorRecursive(activities, start, finish, -1, len(activities))
	print("Activities selected by Iteration method are ", listIter)
	print("with following start and end times")
	printStartAndEndTimes(activities, start, finish, listIter)
	print("\nActivities selected by Recursive method are ", listRecur)
	print("with following start and end times")
	printStartAndEndTimes(activities, start, finish, listRecur)